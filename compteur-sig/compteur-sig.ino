 /*
 * Created:   lun. avr. 12 2021
 * By Pierre Couderc
 * Processor: Arduino Uno
 * Compiler:  Arduino AVR
 */

#include <SoftwareSerial.h>

//définitions Compteur

String stringCPT = "";
String base = "";
String adco = "";
String msg1="";
int base1,adco1;
int envoi=0;

int incomingByte = 0; // for incoming serial data

//Version Arduino UNO
//SoftwareSerial SigfoxSerial(4, 5); // RX, TX

//Version Arduino LEONARDO
//si utilisation Rx entre Akene et Arduino
//relier 4 (Tx Akene)->  8 (Rx Arduino)
SoftwareSerial SigfoxSerial(8, 5); // RX, TX

//---CONFIG_END---

void setup () {
  // initalize Sigfox Serial Port
  SigfoxSerial.begin(9600);
  //Pour Arduino LEONARDO
  //Attention relier 4 (Tx Akene)-> 
  // 8 (Rx Arduino) 
  pinMode(4, INPUT);

  // initalize Compteur Serial Port
  Serial1.begin(1200,SERIAL_7E1);

  // initalize Sigfox Akene
  SigfoxSerial.println("ATE1");
  delay(250);
  SigfoxSerial.println("ATQ0");
  delay(250);

}

void loop() {
  String s = checkCPT();
  if(s && s.substring(0, 4) == "BASE")
  {
      base = s.substring(5,14);
      //Vérification longueur chaine pair ou impair
      base1=base.length();
      base1 = base1 % 2;
      
      //Test
      /*SigfoxSerial.print("Conso : ");
      SigfoxSerial.println(base);
      SigfoxSerial.print("longueur : ");
      SigfoxSerial.println(base.length());*/
  envoi++;
  }
  if(s && s.substring(0, 1) == "A")
  {
      adco = s.substring(5,17);
      //Vérification longueur chaine pair ou impair
      adco1=adco.length();
      adco1 = adco1 % 2;
      
      //Test
      /*SigfoxSerial.print("Compteur : ");
      SigfoxSerial.println(adco);
      SigfoxSerial.print("longueur : ");
      SigfoxSerial.println(adco.length());
      SigfoxSerial.print("parite : ");
      SigfoxSerial.println(adco1);*/
   envoi++;
   }
   //Send Sigfox      
   if(envoi==2)
      {
      SigfoxSerial.print("AT$SF=");
      if (adco1==0)  SigfoxSerial.print(adco);
      else  //rajout 0 si longueur impaire
            {
           SigfoxSerial.print(0);
           SigfoxSerial.print(adco);
           }
      if (base1==0)  SigfoxSerial.println(base);
      else  //rajout 0 si longueur impaire
            {
           SigfoxSerial.print(0);
           SigfoxSerial.println(base);
           }
      envoi=0;
      // Wait 11 minutes 
      delay(660000);
      }

}


// Check Serial and returns string if full line recorded, else false
String checkCPT()
{
  String tmp ="";
  if (Serial1.available())
  {
    char c = Serial1.read();
    if (c != '\n' && c != '\r')
    {
      stringCPT  += c;
    }
    else
    {
      if (stringCPT != "")
      {
        String tmp = stringCPT;
        stringCPT = "";
        return tmp;
      }
    }
  }
  return tmp;
}
