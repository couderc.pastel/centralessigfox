    <?php
    // Afficher les erreurs à l'écran
    ini_set('display_errors', 1);
   // Afficher les erreurs et les avertissements
    error_reporting('e_all');
    ?><!doctype html>
<html>
	<head>
		<?php include 'php/models/head.php' ?>
		 <!-- Rafraichissement de la page toutes les 60s -->
		<meta http-equiv="Refresh" content="60">   
	</head>

	<body>
		<?php include 'php/models/header.php' ?>
		<?php include 'php/models/sidebar.php' ?>
		
		<div class="container">
			<div class="content">
				<div class="table-container">
					<table>
						<thead>
							<tr>
								<th class="thead-title">Date et heure</th>
								<th class="thead-title">Identifiant compteur</th>
								<th class="thead-title">Production cumulée (kWh)</th>
							</tr>
						</thead>

						<tbody>			
							<?php 
			//Ouverture du fichier de données******************************
								//lecture d'un fichier ligne par ligne
								$fic=fopen("../sigfox/fichier1.csv", "r");    //A COMPLETER
								$i=1 ;//Compteur de ligne
								while(!feof($fic))
									{
									$line= fgets($fic,1024);
									$param = explode (";",$line);
									//Enregistrement date et heure
									$heure = explode (":",$param[1]);
									$date= date('Y-m-d H:i:s',$heure[1]);
									//Enregistrement des données
									$donnee = explode (":",$param[2]);
									$data = $donnee[1];
									$adco = substr($data, 1,12);
									$base = substr($data, 13,7);
									$basedec = substr($data, 20,3);
									?>
									<tr>
										<td><?= $date ?></td>
										<td><?= $adco ?></td>
										<td><?= $base ?>.<?= $basedec ?></td>	
									</tr>
									<?php
									}
								fclose($fic) ; 
							?>

							    
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>